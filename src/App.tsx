import { useState, useEffect } from 'react';
import {
  Context,
  Solutions,
  Tray,
  Shape,
  GameEnd
} from './components';
import {
  ShapeModel,
  ShapesInTraysModel,
  SolutionsSolutionModel,
  SolutionsModel,
  GameEndStatus
} from './shared';
import { builder } from './utils';
import './App.css';

export const App = () => {
  const [solutionProperties, setSolutionProperties] = useState([] as SolutionsModel[])
  const [solutions, setSolutions] = useState([] as SolutionsSolutionModel[]);
  const [shapes, setShapes] = useState([] as ShapeModel[]);
  const [shapesInTrays, setShapesInTrays] = useState([] as ShapesInTraysModel[]);
  const [solutionsCompleted, setSolutionsCompleted] = useState(false);
  const [gameEndStatus, setGameEndStatus] = useState(GameEndStatus.DEFAULT);
  const difficulty = 1;

  useEffect(() => {
    if (solutionsCompleted) {
      setGameEndStatus(GameEndStatus.WON);
    }
  }, [solutionsCompleted]);

  useEffect(() => {
    builder(difficulty)
      .then((solutions: SolutionsModel[]) => {
        const traySolutions = solutions.map((solution, index) => {
          return { ...solution.solution, trayId: index + 1 };
        });

        const solutionShapes = solutions.map(solution => {
          return solution.shapes;
        }).flat();

        const modifiedSolutionShapes = solutionShapes.map((solutionShape, index) => {
          return {
            id: solutionShape.id,
            kind: 'rectangle',
            color: '#ffffff',
            position: {
              x: 25,
              y: index === 0 ? 70 : (index * 67) + 70
            },
            positive: solutionShape.positiveColumns.filter(pc => pc === true).length,
            negative: Math.abs(solutionShape.negativeColumns.filter(nc => nc === true).length) * -1,
            positiveColumns: solutionShape.positiveColumns,
            negativeColumns: solutionShape.negativeColumns,
            bg: solutionShape.bg,
            trayId: null
          };
        });

        if (modifiedSolutionShapes.length > 10) {
          setGameEndStatus(GameEndStatus.ERROR);
          console.error('Too many shapes!');
        }

        setSolutionProperties(solutions);
        setSolutions(traySolutions);
        setShapes(modifiedSolutionShapes);
      })
      .catch((error: string) => {
        throw new Error(error);
      })
  }, []);

  return (
    <Context.Provider value={{ shapes, setShapes }}>
      { solutionProperties.length > 0 && solutions.length > 0 && shapes.length > 0 &&
        <div className='App'>
          <div className="headers">
            <h1>Fragment Inventory</h1>
            <h1>Match the Pattern</h1>
          </div>
          <div className="spectrograph">
            <div className="shape-area">
              {
                shapes.map((value: ShapeModel, index: number) => (
                  <Shape id={value.id} key={index} />
                ))
              }
            </div>
            <div className="shapes-and-trays">
              <Solutions
                solutionProperties={solutionProperties}
                solutions={solutions}
                shapesInTrays={shapesInTrays}
                setSolutionsCompleted={setSolutionsCompleted}
              />
              <Tray
                shapes={shapes}
                setShapes={setShapes}
                setShapesInTrays={setShapesInTrays}
              />
            </div>
          </div>

          {
            gameEndStatus !== GameEndStatus.DEFAULT &&
            <GameEnd gameEndStatus={gameEndStatus} />
          }
        </div>
      }
    </Context.Provider>
  );
};
