/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext } from 'react';

interface ShapeContext {
  shapes: any[];
  setShapes: any;
}

export const Context = createContext<ShapeContext>(undefined as any);
