import { GameEndStatus } from '../../shared';
import './gameend.css';

export const GameEnd = ({
  gameEndStatus
}: {
  gameEndStatus: string;
}) => {
  return (
    <div className='game-end'>
      {
        gameEndStatus === GameEndStatus.WON &&
        <div className='status won'>Winner!</div>
      }
      {
        gameEndStatus === GameEndStatus.ERROR &&
        <div className='status error'>ERROR!</div>
      }
    </div>
  );
};
