import { useContext, useRef, useState, useLayoutEffect } from 'react';
import Draggable, { DraggableData, DraggableEvent } from 'react-draggable';
import { Context } from '../Context/Context';
import { ShapeModel, ShapePositionModel } from '../../shared';
import './shape.css';

export const Shape = (
  {
    id
  }: {
    id: number
  }
) => {
  const nodeRef = useRef(null);
  const { shapes, setShapes } = useContext(Context);
  const [myShape, setMyShape] = useState({} as ShapeModel);
  const [visible, setVisible] = useState(false);
  const [pos, setPos] = useState({ x: 0, y: 0 })
  
  const handleControlledDragStop = (_e: DraggableEvent, position: DraggableData) => {
    setVisible(false);
    const { x, y } = position;
    let trayCoords: ShapePositionModel = { x, y };
    const newShapes = shapes.map((shape: ShapeModel) => {
      if (id === shape.id) {
        const tray1 = document.querySelector(`#tray1`);
        const tray2 = document.querySelector(`#tray2`);
        const tray3 = document.querySelector(`#tray3`);

        if (tray1 && tray2 && tray3) {
          const tray1coords = tray1.getBoundingClientRect();
          const tray2coords = tray2.getBoundingClientRect();
          const tray3coords = tray3.getBoundingClientRect();

          const shapesInSameTray: Element[] = [];

          if (shape.position.x !== undefined && shape.position.y !== undefined) {
            const newX = 450 / 2 + x;
            if (newX > tray1coords!.left && newX < tray1coords!.right) {
              if (y > tray1coords!.y && y < tray1coords!.bottom) {
                trayCoords = { x: Math.floor(tray1coords!.x + 7), y: Math.floor(tray1coords!.y + 15) };

                const allShapes = document.querySelectorAll('.shape');

                if (allShapes?.length > 0) {
                  allShapes.forEach(s => {
                    const shapeCoords = s.getBoundingClientRect();
                    if (shapeCoords.x === trayCoords.x) {
                      shapesInSameTray.push(s);
                    }
                  });
                }
              }
            }

            if (newX > tray2coords!.left && newX < tray2coords!.right) {
              if (y > tray2coords!.y && y < tray2coords!.bottom) {
                trayCoords = { x: Math.floor(tray2coords!.x + 7), y: Math.floor(tray2coords!.y + 15) };

                const allShapes = document.querySelectorAll('.shape');

                if (allShapes?.length > 0) {
                  allShapes.forEach(s => {
                    const shapeCoords = s.getBoundingClientRect();
                    if (shapeCoords.x === trayCoords.x) {
                      shapesInSameTray.push(s);
                    }
                  });
                }
              }
            }

            if (newX > tray3coords!.left && newX < tray3coords!.right) {
              if (y > tray3coords!.y && y < tray3coords!.bottom) {
                trayCoords = { x: Math.floor(tray3coords!.x + 7), y: Math.floor(tray3coords!.y + 15) };

                const allShapes = document.querySelectorAll('.shape');

                if (allShapes?.length > 0) {
                  allShapes.forEach(s => {
                    const shapeCoords = s.getBoundingClientRect();
                    if (shapeCoords.x === trayCoords.x) {
                      shapesInSameTray.push(s);
                    }
                  });
                }
              }
            }

            if (shapesInSameTray?.length > 0) {
              const yValues = shapesInSameTray.map(s => {
                const shapeCoords = s.getBoundingClientRect();
                return shapeCoords.y;
              }).sort();

              switch (yValues.length) {
                case 2: {
                  if (y < yValues[0]) {
                    const distance = yValues[0] - 190;
                    if (distance > 150) {
                      trayCoords.y = Math.floor(yValues[0] - 125);
                    }
                  } else if (y > yValues[yValues.length - 1]) {
                    const distance = 680 - yValues[yValues.length - 1];
                    if (distance > 150) {
                      trayCoords.y = Math.floor(yValues[yValues.length - 1] + 125);
                    }
                  } else {
                    const distance = yValues[1] - yValues[0];
                    if (distance > 150) {
                      trayCoords.y = Math.floor(yValues[0] + 125);
                    } else {
                      trayCoords.y = Math.floor(yValues[yValues.length - 1] + 125);
                    }
                  }
                  break;
                }
                case 3: {
                  if (y < yValues[0]) {
                    const distance = yValues[0] - 190;
                    if (distance > 150) {
                      trayCoords.y = Math.floor(yValues[0] - 125);
                    }
                  } else if (y > yValues[yValues.length - 1]) {
                    if (y > 680) {
                      trayCoords = { x: Math.floor(myShape.position.x), y: Math.floor(myShape.position.y) };
                    } else {
                      const distance = 680 - yValues[yValues.length - 1];
                      if (distance > 150) {
                        trayCoords.y = Math.floor(yValues[yValues.length - 1] + 125);
                      }
                    }
                  } else {
                    const distance1 = yValues[1] - yValues[0];
                    const distance2 = yValues[2] - yValues[1];
                    if (distance1 > 150) {
                      trayCoords.y = Math.floor(yValues[0] + 125);
                    } else if (distance2 > 150) {
                      trayCoords.y = Math.floor(yValues[1] + 125);
                    } else {
                      trayCoords.y = Math.floor(yValues[yValues.length - 1] + 125);
                    }
                  }
                  break;
                }
                case 4: {
                  if (y < yValues[0]) {
                    const distance = yValues[0] - 190;
                    if (distance > 150) {
                      trayCoords.y = Math.floor(yValues[0] - 125);
                    }
                  } else if (y > yValues[yValues.length - 1]) {
                    const distance = 700 - yValues[yValues.length - 1];
                    if (distance > 150) {
                      trayCoords.y = Math.floor(yValues[yValues.length - 1] + 125);
                    }
                  } else {
                    const distance1 = yValues[1] - yValues[0];
                    const distance2 = yValues[2] - yValues[1];
                    const distance3 = yValues[3] - yValues[2];
                    if (distance1 > 150) {
                      trayCoords.y = Math.floor(yValues[0] + 125);
                    } else if (distance2 > 150) {
                      trayCoords.y = Math.floor(yValues[1] + 125);
                    } else if (distance3 > 150) {
                      trayCoords.y = Math.floor(yValues[2] + 125);
                    }
                  }
                  break;
                }
                default: {
                  if (y < yValues[0]) {
                    const distance = yValues[0] - 190;
                    if (distance > 150) {
                      trayCoords.y = Math.floor(yValues[0] - 125);
                    }
                  } else {
                    trayCoords.y = Math.floor(yValues[yValues.length - 1] + 125);
                  }
                  break;
                }
              }

              if (yValues.length > 1 && yValues.filter(val => val === trayCoords.y).length > 0) {
                trayCoords = { x: Math.floor(myShape.position.x), y: Math.floor(myShape.position.y) };
              }
            }

            if (trayCoords?.y < 190 || trayCoords?.y > 680 || y <= 190 || x > tray3coords.right || x < 0 || y > tray1coords.bottom || y < 0) {
              trayCoords = { x: Math.floor(myShape.position.x), y: Math.floor(myShape.position.y) };
            }
          }
        }

        setPos({ x: trayCoords ? trayCoords.x : x, y: trayCoords ? trayCoords.y : y });

        return {
          id: myShape.id,
          kind: myShape.kind,
          color: myShape.color,
          position: { x: trayCoords.x, y: trayCoords.y },
          positive: myShape.positive,
          negative: myShape.negative,
          positiveColumns: myShape.positiveColumns,
          negativeColumns: myShape.negativeColumns,
          trayId: myShape.trayId
        };
      }

      return shape;
    });
    setShapes(newShapes);
  };

  useLayoutEffect(() => {
    shapes.forEach((shape: ShapeModel) => {
      if (id === shape.id) {
        setMyShape(shape);
        setPos({ x: shape.position.x, y: shape.position.y });
        setVisible(true);
      }
    });
  }, [id, shapes]);


  return (
    <>
      { myShape && visible &&
        <Draggable
          position={pos}
          onStop={handleControlledDragStop}
          nodeRef={nodeRef}
        >
          <div
            className={`shape ${myShape.kind}`}
            style={{
              backgroundColor: `${myShape.bg}`,
              boxShadow: '0px -5px 10px 1px #000'
            }}
            ref={nodeRef}
          >
            {myShape.positiveColumns && myShape.negativeColumns && myShape.positiveColumns.map((pc, index) => {
              if (myShape.negativeColumns.indexOf(true) === index) {
                return <div className="column enabled negative" key={index}></div>
              }
              
              return <div className={`${pc ? 'column enabled positive' : 'column'}`} key={index}></div>
            })}
          </div>
        </Draggable>
      }
    </>
  );
};
