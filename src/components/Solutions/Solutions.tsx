import React, { useState, useRef, useEffect } from 'react';
import { ShapesInTraysModel, SolutionsModel, SolutionsSolutionModel } from '../../shared';
import './solutions.css';

export const Solutions = ({
  solutionProperties,
  solutions,
  shapesInTrays,
  setSolutionsCompleted
}: {
  solutionProperties: SolutionsModel[];
  solutions: SolutionsSolutionModel[];
  shapesInTrays: ShapesInTraysModel[];
  setSolutionsCompleted: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const [traysCompleted, setTraysCompleted] = useState([
    { trayId: 1, completed: false },
    { trayId: 2, completed: false },
    { trayId: 3, completed: false }
  ]);
  const solutionTotals = useRef(solutionProperties.map((properties, index) => ({ trayId: index + 1, total: properties.total })));
  const userTotals = useRef([
    { trayId: 1, total: 0 },
    { trayId: 2, total: 0 },
    { trayId: 3, total: 0 }
  ]);

  useEffect(() => {
    let updatedTraysCompleted = [...traysCompleted];

    if (solutionTotals.current[0].total === userTotals.current[0].total) {
      updatedTraysCompleted = updatedTraysCompleted.map(tc => {
        if (tc.trayId === solutionTotals.current[0].trayId) {
          return { trayId: tc.trayId, completed: true };
        }

        return tc;
      });
    }

    if (solutionTotals.current[1].total === userTotals.current[1].total) {
      updatedTraysCompleted = updatedTraysCompleted.map(tc => {
        if (tc.trayId === solutionTotals.current[1].trayId) {
          return { trayId: tc.trayId, completed: true };
        }

        return tc;
      });
    }

    if (solutionTotals.current[2].total === userTotals.current[2].total) {
      updatedTraysCompleted = updatedTraysCompleted.map(tc => {
        if (tc.trayId === solutionTotals.current[2].trayId) {
          return { trayId: tc.trayId, completed: true };
        }

        return tc;
      });
    }

    if (updatedTraysCompleted.filter(utc => utc.completed === true).length === 3) {
      setSolutionsCompleted(true);
    }

    setTraysCompleted(updatedTraysCompleted);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shapesInTrays, solutionProperties]);

  const checkNegativeColumns = (combinedNegativeColumns: boolean[], negativeColumns: boolean[][]) => {
    combinedNegativeColumns = [...negativeColumns[0]];

    // We will loop through the multidimensional array now...
    negativeColumns.forEach((nc, ncIndex) => {
      // We will ignore the first array child because it was assigned to "combinedNegativeColumns" above
      if (ncIndex > 0) {
        // We will loop through the booleans for all arrays that aren't the first array in "negativeColumns"
        nc.forEach((ncInner, ncInnerIndex) => {
          // If this boolean is true, and then we locate the same index of the final array; if this indexed item is false, then do the following...
          if (ncInner && !combinedNegativeColumns[ncInnerIndex]) {
            // Replace the final array with our true boolean
            combinedNegativeColumns.splice(ncInnerIndex, 1, ncInner);
          }
        });
      }
    });

    return combinedNegativeColumns;
  };

  return (
    <div className='solutions'>
      {solutions && solutions.map((solution, solutionIndex) => (
        <div
          className={`solution ${traysCompleted.find(tc => tc.trayId === solution.trayId && tc.completed) ? 'completed' : ''}`}
          id={`solution-${solution.trayId}`}
          key={solutionIndex}
        >
          {solution.enabledColumns.map((enabledColumn, enabledColumnIndex) => {
            let hasMatchingColumn = false;
            let extraColumn = false;
            let negateExtraColumn = false;
            let combinedPositiveColumns: boolean[] = [];
            let combinedNegativeColumns: boolean[] = [];

            // Get the shapes that are in this solution's tray
            const shapesMatchingTray = shapesInTrays.filter(sit => sit.trayId === solution.trayId);
            if (shapesMatchingTray.length > 0) {
              shapesMatchingTray.forEach(smt => {
                // This will come out to be a multidimensional array because the "positiveColumns" property inside "shape" is an array of booleans
                // All "child arrays" will have exactly 16 booleans
                const positiveColumns = smt.shapes.map(shape => shape.positiveColumns);
                
                // Now get the negative columns
                // The same functionality for the positive columns appears here
                const negativeColumns = smt.shapes.map(shape => shape.negativeColumns);

                if (positiveColumns.length > 0) {
                  // We are going to make a final array that we will end up combining all arrays into
                  // We use the first indexed array as our initial array that will be modified
                  combinedPositiveColumns = [...positiveColumns[0]];

                  // We will loop through the multidimensional array now...
                  positiveColumns.forEach((pc, pcIndex) => {
                    // We will ignore the first array child because it was assigned to "combinedPositiveColumns" above
                    if (pcIndex > 0) {
                      // We will loop through the booleans for all arrays that aren't the first array in "positiveColumns"
                      pc.forEach((pcInner, pcInnerIndex) => {
                        // If this boolean is true, and then we locate the same index of the final array; if this indexed item is false, then do the following...
                        if (pcInner && !combinedPositiveColumns[pcInnerIndex]) {
                          // Replace the final array with our true boolean
                          combinedPositiveColumns.splice(pcInnerIndex, 1, pcInner);
                        }
                      });
                    }
                  });

                  // Now we check if the final array's indexed item is true, and if it is, we can turn this column in the solution to white
                  if (enabledColumn && combinedPositiveColumns[enabledColumnIndex] === true) {
                    hasMatchingColumn = true
                  } else if (!enabledColumn && combinedPositiveColumns[enabledColumnIndex] === true) {
                    extraColumn = true;

                    if (negativeColumns.length > 0) {
                      // We are going to make a final array that we will end up combining all arrays into
                      // We use the first indexed array as our initial array that will be modified
                      combinedNegativeColumns = [...negativeColumns[0]];

                      // We will loop through the multidimensional array now...
                      negativeColumns.forEach((nc, ncIndex) => {
                        // We will ignore the first array child because it was assigned to "combinedNegativeColumns" above
                        if (ncIndex > 0) {
                          // We will loop through the booleans for all arrays that aren't the first array in "negativeColumns"
                          nc.forEach((ncInner, ncInnerIndex) => {
                            // If this boolean is true, and then we locate the same index of the final array; if this indexed item is false, then do the following...
                            if (ncInner && !combinedNegativeColumns[ncInnerIndex]) {
                              // Replace the final array with our true boolean
                              combinedNegativeColumns.splice(ncInnerIndex, 1, ncInner);
                            }
                          });
                        }
                      });

                      if (combinedNegativeColumns[enabledColumnIndex]) {
                        negateExtraColumn = true;
                      }
                    }
                  }
                }

                userTotals.current = userTotals.current.map(ut => {
                if (solution.trayId === ut.trayId && (combinedPositiveColumns.length > 0 || combinedNegativeColumns.length > 0)) {
                    const verifyNegativeColumns = checkNegativeColumns(combinedNegativeColumns, negativeColumns);
                    const positiveColumnsTotal = combinedPositiveColumns.filter(pc => pc === true).length;
                    const negativeColumnsTotal = verifyNegativeColumns.filter(nc => nc === true).length;
                    const total = positiveColumnsTotal - negativeColumnsTotal;
                    return { trayId: ut.trayId, total };
                  }

                  return ut;
                });
              });
            }

            return (
              <React.Fragment key={(Math.floor(Math.random() * 99999) + 1)}>
                {
                  enabledColumn 
                  ? hasMatchingColumn
                    ? <div
                        className={`column column-${enabledColumnIndex} enabled matched`}
                        key={enabledColumnIndex}
                      ></div>
                    : <div
                        className={`column column-${enabledColumnIndex} enabled`}
                        key={enabledColumnIndex}
                      ></div>
                  : extraColumn 
                    ? <div
                        className={`column column-${enabledColumnIndex} extra ${negateExtraColumn ? 'negate' : ''}`}
                        key={enabledColumnIndex}
                      ></div>
                    : <div
                        className={`column column-${enabledColumnIndex}`}
                        key={enabledColumnIndex}
                      ></div>
                }
              </React.Fragment>
            );
          })}
        </div>
      ))}
    </div>
  );
};
