import { useRef, useEffect } from 'react';
import { ShapeModel, ShapesInTraysModel } from '../../shared';
import './tray.css';

export const Tray = ({
  shapes,
  setShapes,
  setShapesInTrays
}: {
  shapes: ShapeModel[];
  setShapes: React.Dispatch<React.SetStateAction<ShapeModel[]>>;
  setShapesInTrays: React.Dispatch<React.SetStateAction<ShapesInTraysModel[]>>;
}) => {
  const tray1 = useRef<HTMLDivElement>(null);
  const tray2 = useRef<HTMLDivElement>(null);
  const tray3 = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const tray1coords = tray1.current?.getBoundingClientRect();
    const tray1Height = tray1.current?.clientHeight;
    const tray1Width = tray1.current?.clientWidth;

    const tray2coords = tray2.current?.getBoundingClientRect();
    const tray2Height = tray2.current?.clientHeight;
    const tray2Width = tray2.current?.clientWidth;

    const tray3coords = tray3.current?.getBoundingClientRect();
    const tray3Height = tray3.current?.clientHeight;
    const tray3Width = tray3.current?.clientWidth;

    const verifyRefVars = (coords: DOMRect | undefined, height: number | undefined, width: number | undefined) => {
      if (coords && coords.x !== undefined && coords.y !== undefined && height && width) {
        return true;
      }

      return false;
    }

    if (
      verifyRefVars(tray1coords, tray1Height, tray1Width) && 
      verifyRefVars(tray2coords, tray2Height, tray2Width) && 
      verifyRefVars(tray3coords, tray3Height, tray3Width)
    ) {
      const tray1Obj: ShapesInTraysModel = { trayId: 1, shapes: [] };
      const tray2Obj: ShapesInTraysModel = { trayId: 2, shapes: [] };
      const tray3Obj: ShapesInTraysModel = { trayId: 3, shapes: [] };

      shapes.forEach(shape => {
        if (shape.position.x !== undefined && shape.position.y !== undefined) {
          const { x, y } = shape.position;
          if (x > tray1coords!.x && x < (tray1coords!.x + tray1Width!)) {
            if (y > tray1coords!.y && y < (tray1coords!.y + tray1Height!)) {
              tray1Obj.shapes.push({ ...shape, trayId: 1 });
            }
          }

          if (x > tray2coords!.x && x < (tray2coords!.x + tray2Width!)) {
            if (y > tray2coords!.y && y < (tray2coords!.y + tray2Height!)) {
              tray2Obj.shapes.push({ ...shape, trayId: 2 });
            }
          }

          if (x > tray3coords!.x && x < (tray3coords!.x + tray3Width!)) {
            if (y > tray3coords!.y && y < (tray3coords!.y + tray3Height!)) {
              tray3Obj.shapes.push({ ...shape, trayId: 3 });
            }
          }
        }
      });

      setShapesInTrays([tray1Obj, tray2Obj, tray3Obj]);
    }
  }, [shapes, setShapes, setShapesInTrays]);

  return (
    <div className='trays'>
      <div ref={tray1} className="tray" id="tray1"></div>
      <div ref={tray2} className="tray" id="tray2"></div>
      <div ref={tray3} className="tray" id="tray3"></div>
    </div>
  );
};
