export const GameEndStatus = {
  DEFAULT: 'idle',
  WON: 'won',
  LOST: 'lost',
  ERROR: 'error'
};
