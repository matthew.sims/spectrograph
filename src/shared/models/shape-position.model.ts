export interface ShapePositionModel {
  x: number;
  y: number;
}
