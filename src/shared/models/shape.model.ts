import { ShapePositionModel } from './shape-position.model';

export interface ShapeModel {
  id: number;
  kind: string;
  color: string;
  position: ShapePositionModel;
  positive: number;
  negative: number;
  positiveColumns: boolean[];
  negativeColumns: boolean[];
  bg: string;
  trayId: number | null;
}
