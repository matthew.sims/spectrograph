import { ShapeModel } from './shape.model';

export interface ShapesInTraysModel {
  trayId: number;
  shapes: ShapeModel[];
}
