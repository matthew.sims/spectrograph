export interface SolutionsSolutionModel {
  enabledColumns: boolean[];
  trayId?: number;
}

export interface SolutionsShapesModel {
  id: number;
  positiveColumns: boolean[];
  negativeColumns: boolean[];
  bg: string;
}

export interface SolutionsModel {
  difficulty: number;
  total: number;
  solution: SolutionsSolutionModel;
  shapes: SolutionsShapesModel[];
}

export interface ExtraPositiveColumnsModel {
  shapeIndex: number;
  columnIndex: number;
}

export interface FalsePositiveColumnsModel {
  extraPositiveColumns: ExtraPositiveColumnsModel[];
  modifiedShapes: SolutionsShapesModel[];
}
