export interface TrayModel {
  height: number;
  width: number;
  x: number;
  y: number;
}
