import { SolutionsModel, MAX_NUMBER_OF_SOLUTIONS } from '../shared';
import { solutions, generateSolutions } from './solutions';

const getSolutions = (difficulty: number) => solutions.filter(solution => solution.difficulty === difficulty);

export const builder = (difficulty: number): Promise<SolutionsModel[]> => new Promise((resolve, reject) => {
  /*
    Uncomment to build solutions
    NOTE: You will need to manually create the false positive columns
    const maxRepeat = 50;
    const maxTrueValues = 10;
    const columnCount = 16;
    const testSolutions = generateSolutions(maxRepeat, maxTrueValues, columnCount);
  */
  
  const matchingSolutions = getSolutions(difficulty);

  const randomSolutions: SolutionsModel[] = [];
  let remainingSolutions = MAX_NUMBER_OF_SOLUTIONS;
  do {
    const randomNumber = Math.floor(Math.random() * matchingSolutions.length);
    const foundSolution = matchingSolutions[randomNumber];
    randomSolutions.push(foundSolution);
    remainingSolutions--;
  } while (remainingSolutions > 0)

  if (matchingSolutions.length > 0) {
    resolve(matchingSolutions);
  } else {
    reject('No matching solutions could be found');
  }
});
