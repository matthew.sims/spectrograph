import { SolutionsModel, SolutionsShapesModel } from '../shared';

const getRandomID = () => (Math.floor(Math.random() * 999999) + 1);

const getRandomColor = () => {
  const possibleColors = ['#00ff00', '#0000ff', '#ffff00', '#ff00ff', '#990000', '#009900', '#000099', '#999900'];
  const randomNumber = Math.floor(Math.random() * possibleColors.length);
  return possibleColors[randomNumber];
};

function generateEnabledColumns(maxTrueValues: number, columnCount: number) {
  let rngTrueCount = (Math.floor(Math.random() * maxTrueValues) + 1);

  return new Array(columnCount).fill(false).map(column => {
    const rngTrueOrFalse = (Math.floor(Math.random() * 2) + 1);

    if (rngTrueCount > 0 && rngTrueOrFalse === 2) {
      rngTrueCount = rngTrueCount - 1;
      return true;
    }

    return column;
  });
}

function generateDifficulty() {
  return 1;
}

function generateTotal(enabledColumns: boolean[]) {
  return enabledColumns.filter(column => column === true).length;
}

function generatePositiveColumns(columnCount: number, selectedTrueIndexes: number[]) {
  const positiveColumns = new Array(columnCount).fill(false);

  return positiveColumns.map((column, index) => {
    if (selectedTrueIndexes.indexOf(index) > -1) {
      return true;
    }

    return column;
  });
}

function generateShapes(columnCount: number, enabledColumns: boolean[], total: number) {
  const shapes: SolutionsShapesModel[] = [];
  const rngShapeCount = Math.floor(Math.random() * 4) + 1;
  // eslint-disable-next-line prefer-const
  let enabledColumnsTrueIndexes = enabledColumns.map((column, index) => {
    if (column === true) {
      return index;
    }
  }).filter(column => column !== undefined) as number[];
  let totalCounter = total;

  for (let a = 0; a < rngShapeCount; a++) {
    if (rngShapeCount !==  1) {
      if (a !== (rngShapeCount - 1)) {
        totalCounter = 1;
      } else {
        totalCounter = enabledColumnsTrueIndexes.length;
      }
    }

    const selectedTrueIndexes: number[] = [];
    for (let b = 0; b < totalCounter; b++) {
      let chosenTrueIndex;
      let found = false;
      do {
        const rngIndex = Math.floor(Math.random() * enabledColumnsTrueIndexes.length);
        chosenTrueIndex = enabledColumnsTrueIndexes[rngIndex] ?? enabledColumnsTrueIndexes[rngIndex - 1];

        if (selectedTrueIndexes.indexOf(chosenTrueIndex) < 0) {
          found = true;
        }
      } while (!found);

      selectedTrueIndexes.push(chosenTrueIndex);
    }

    enabledColumnsTrueIndexes = enabledColumnsTrueIndexes.filter(trueIndex => selectedTrueIndexes.indexOf(trueIndex) < 0);

    const positiveColumns = generatePositiveColumns(columnCount, selectedTrueIndexes);
    const negativeColumns = new Array(columnCount).fill(false);

    shapes.push({
      id: getRandomID(),
      bg: getRandomColor(),
      positiveColumns,
      negativeColumns,
    });
  }

  return shapes.filter(shape => !shape.positiveColumns.every(pc => pc === false))
}

function startGenerator(maxTrueValues: number, columnCount: number) {
  const enabledColumns = generateEnabledColumns(maxTrueValues, columnCount);
  const difficulty = generateDifficulty();
  const total = generateTotal(enabledColumns);
  const shapes = generateShapes(columnCount, enabledColumns, total);

  return {
    difficulty,
    total,
    solution: {
      enabledColumns,
    },
    shapes
  };
}

export const generateSolutions = (maxRepeat: number, maxTrueValues: number, columnCount: number) => {
  const solutions: SolutionsModel[] = [];

  for (let a = 0; a < maxRepeat; a++) {
    const solution: SolutionsModel = startGenerator(maxTrueValues, columnCount);
    solutions.push(solution);
  }

  console.log(JSON.stringify(solutions));
  return solutions;
};

export const solutions: SolutionsModel[] = [];